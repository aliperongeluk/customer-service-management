const express = require('express');
const routes = express.Router();
const request = require('request-promise');

const states = require('../environment/constants/states');
const events = require('../environment/constants/events');
const Ticket = require('../models/serviceticket.model');
const messageSender = require('../eventhandlers/message.sender');

/**
 * @swagger
 * /api/customer-service-management/tickets:
 *    get:
 *      tags:
 *        - tickets
 *      description: Returns all tickets
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Returns array of tickets
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Ticket'
 *        400:
 *          description: Something went wrong
 */
routes.get('/', async (req, res) => {
  try {
    const tickets = await Ticket.find();
    res.send(tickets);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    res.status(400).send('Something went wrong');
  }
});

/**
 * @swagger
 * /api/customer-service-management/tickets/{id}:
 *    get:
 *      tags:
 *        - tickets
 *      description: Returns all tickets
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: path
 *          name: id
 *          description: The identifier of a ticket
 *          schema:
 *            type: string
 *      responses:
 *        200:
 *          description: Returns array of tickets
 *          schema:
 *            $ref: '#/definitions/Ticket'
 *        400:
 *          description: Something went wrong
 */
routes.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const ticketMongo = await Ticket.findById(id);
    const ticket = ticketMongo.toObject();
    ticket.product = await request({
      method: 'GET',
      uri: `http://zuul-apigateway:5005/api/product-catalog-management/products/${
        ticket.product
      }`,
      json: true,
    });
    res.send(ticket);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    res.status(400).send('Something went wrong');
  }
});

/**
 * @swagger
 * /api/customer-service-management/tickets:
 *    post:
 *      tags:
 *        - tickets
 *      description: This should create an object in mongodb
 *      parameters:
 *        - in: body
 *          name: body
 *          description: Create new order object.
 *          schema:
 *              $ref: '#/definitions/Ticket'
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: successfully added a ticket
 *          schema:
 *              $ref: '#/definitions/Ticket'
 *        400:
 *          description: could not create ticket
 *          schema:
 *            type: object
 *            properties:
 *              error:
 *                type: string
 */
routes.post('/', (req, res) => {
  const ticket = new Ticket(req.body);

  ticket
    .save()
    .then(ticket => {
      messageSender.publish(events.TICKET_CREATED, ticket);
      res.status(200).send(ticket);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/customer-service-management/tickets/{id}:
 *    put:
 *      tags:
 *        - tickets
 *      description: This should create an object in mongodb
 *      parameters:
 *        - in: path
 *          name: id
 *          description: The identifier of a ticket
 *          schema:
 *            type: string
 *        - in: body
 *          name: body
 *          description: Create new order object.
 *          schema:
 *            $ref: '#/definitions/Ticket'
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: successfully added a ticket
 *          schema:
 *              $ref: '#/definitions/Ticket'
 *        400:
 *          description: could not create ticket
 *          schema:
 *            type: object
 *            properties:
 *              error:
 *                type: string
 */
routes.put('/:id', (req, res) => {
  const { id } = req.params;
  const { state } = req.body;

  Ticket.findByIdAndUpdate(id, { state: state }, { new: true })
    .then(ticket => {
      if (state === states.UPDATED) {
        messageSender.publish(events.TICKET_UPDATED, ticket);
      } else if (state === states.SOLVED) {
        messageSender.publish(events.TICKET_SOLVED, ticket);
      }
      res.status(200).send(ticket);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

module.exports = routes;
