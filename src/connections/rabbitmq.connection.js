// LOCAL: amqp://localhost
// SERVER: amqp://AliB:perongelukexpres@rabbitmq

const amqp = require('amqplib');
let counter = 0;

const constants = require('../environment/constants/constants');

const connect = () => {
  return new Promise((resolve, reject) => {
    amqp
      .connect('amqp://localhost')
      .then(connection => {
        counter = 0;

        const createReceiveChannel = new Promise((resolve, reject) => {
          connection
            .createChannel()
            .then(receiveChannel => {
              resolve(receiveChannel);
            })
            .catch(error => {
              reject(error);
            });
        });

        const createSendChannel = new Promise((resolve, reject) => {
          connection
            .createChannel()
            .then(sendChannel => {
              resolve(sendChannel);
            })
            .catch(error => {
              reject(error);
            });
        });

        Promise.all([createReceiveChannel, createSendChannel]).then(
          channels => {
            channels.forEach(channel => {
              channel.assertExchange(constants.EXCHANGE, 'topic', {
                durable: true,
              });
            });

            resolve({
              receiveChannel: channels[0],
              sendChannel: channels[1],
            });
          }
        );
      })
      .catch(error => {
        if (counter === 10) {
          reject(error);
        }

        setTimeout(() => {
          counter++;
          connect();
        }, 3000);
      });
  });
};

module.exports = { connect };
