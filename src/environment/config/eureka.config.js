const ip = require('ip');
const Eureka = require('eureka-js-client').Eureka;

const ipAddr = process.env.IP_ADDRESS || ip.address();
const hostName = process.env.HOST_NAME || ip.address();
const port = process.env.PORT || 7005;

const eurekaClient = new Eureka({
  // application instance information
  instance: {
    app: 'customer-service-management',
    hostName: hostName,
    ipAddr: ipAddr,
    homePagekUrl: `http://${ipAddr}:${port}`,
    healthCheckUrl: `http://${ipAddr}:${port}/actuator/health`,
    port: {
      $: port,
      '@enabled': 'true',
    },
    vipAddress: 'customer-service-management',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    leaseInfo: {
      renewalIntervalInSecs: 10,
      durationInSecs: 30,
    },
  },
  eureka: {
    preferIpAddress: true,
    maxRetries: 30,
    registerWithEureka: true,
    fetchRegistry: true,
    serviceUrls: {
      default: [
        'http://eurekaserver-1:8761/eureka/apps/',
        'http://eurekaserver-2:8762/eureka/apps/',
      ],
    },
  },
});

module.exports = { eurekaClient };
