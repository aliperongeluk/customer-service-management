const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Customer Service Management',
      version: '1.0.0',
      description:
        'Swagger documentation for Customer Service Management microservice (AliPerOngeluk).',
    },
  },

  apis: ['./src/routes/*.js', './src/models/*.js'],
};

const specs = swaggerJsDoc(options);

module.exports = specs;
