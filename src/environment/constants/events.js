const events = {
  TICKET_CREATED: 'TICKET_CREATED',
  TICKET_UPDATED: 'TICKET_UPDATED',
  TICKET_SOLVED: 'TICKET_SOLVED',
};

module.exports = events;
