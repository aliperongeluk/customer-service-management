const constants = {
  CREATED: 'CREATED',
  UPDATED: 'UPDATED',
  SOLVED: 'SOLVED',
};

module.exports = constants;
