const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const states = require('../environment/constants/states');

/**
 * @swagger
 * definitions:
 *  Ticket:
 *    type: object
 *    properties:
 *      name:
 *        type: string
 *      description:
 *        type: string
 *      customer:
 *        type: string
 *      product:
 *        type: string
 *      state:
 *        type: string
 *        enum: ['CREATED', 'UPDATED', 'SOLVED']
 *
 */
const TicketSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'The name of this ticket is required.'],
    },
    description: {
      type: String,
      required: [true, 'Provide a description for the issue.'],
    },
    customer: {
      type: mongoose.Schema.Types.ObjectId,
    },
    state: {
      type: String,
      enum: [states.CREATED, states.UPDATED, states.SOLVED],
      required: [true, 'Ticket should always have a state.'],
    },
    product: {
      type: mongoose.Schema.Types.ObjectId,
    },
  },
  {
    timestamps: true,
  }
);

const Ticket = mongoose.model('ticket', TicketSchema);

module.exports = Ticket;
