const constants = require('../environment/constants/constants');
const events = require('../environment/constants/events');

let messageChannel;

const startConsuming = () => {
  messageChannel.assertExchange(constants.EXCHANGE, 'topic', {
    durable: true,
  });

  consumeTicketCreated(messageChannel);
  consumeTicketUpdated(messageChannel);
  consumeTicketSolved(messageChannel);
};

const consumeTicketCreated = () => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.TICKET_UPDATED}`, {
      exclusive: true,
    })
    .then(queue => {
      messageChannel.bindQueue(queue.queue, constants.EXCHANGE, 'create'); // of 'created'?

      messageChannel.consume(queue.queue, () => {}, {
        noAck: true,
      });
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consumeTicketUpdated = () => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.TICKET_UPDATED}`, {
      exclusive: true,
    })
    .then(queue => {
      messageChannel.bindQueue(queue.queue, constants.EXCHANGE, 'update');

      messageChannel.consume(queue.queue, () => {}, {
        noAck: true,
      });
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consumeTicketSolved = () => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.TICKET_SOLVED}`, {
      exclusive: true,
    })
    .then(queue => {
      messageChannel.bindQueue(queue.queue, constants.EXCHANGE, 'solve');
      messageChannel.consume(queue.queue, () => {}, {
        noAck: true,
      });
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const setMessageChannel = channel => {
  messageChannel = channel;
  startConsuming();
};

module.exports = { setMessageChannel };
