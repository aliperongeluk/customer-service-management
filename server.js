// eslint-disable-next-line no-unused-vars
const mongo = require('./src/connections/mongodb.connection');
const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger.config');

const express = require('express');
const app = express();
const port = process.env.PORT || 7005;

const serviceTicketRoutes = require('./src/routes/serviceticket.routes');
const messageSender = require('./src/eventhandlers/message.sender');

const bodyParser = require('body-parser');

const rabbitmq = require('./src/connections/rabbitmq.connection');
const { eurekaClient } = require('./src/environment/config/eureka.config');

app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/tickets', serviceTicketRoutes);

// default:
app.use('*', (req, res) => {
  res.status(404).send({
    error: 'not available',
  });
});

rabbitmq
  .connect()
  .then(channels => {
    messageSender.setMessageChannel(channels.sendChannel);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.logger.level('debug');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
